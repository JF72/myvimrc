set nu
au BufNewFile,BufRead *.ejs set filetype=html
au BufNewFile,BufRead *.tic set filetype=lua
au BufNewFile,BufRead *.p8 set filetype=lua

set tabstop=4
set expandtab
set shiftwidth=4
set autoindent
set smartindent
set cindent

